﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Media;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telegram.Bot;

namespace AtlanticWritersBidder
{
    public partial class Form1 : Form
    {
        private IWebDriver driver;
        private bool isRunning = true;
        public Form1()
        {
            InitializeComponent();
        }

        public void SendTelegramMessage(string message)
        {
            var botToken = "6795715882:AAFO1XNKqTfjMdrcTeDriQNQJn289zFtYv0";
            var chatId = "734426507";

            try
            {
                var botClient = new TelegramBotClient(botToken);
                botClient.SendTextMessageAsync(chatId, message).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error sending Telegram message: {ex.Message}");
            }
        }


        private void InitializeDriver()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            options.AddArgument("--disable-infobars");
            options.AddArgument("--disable-popup-blocking");
            driver = new ChromeDriver(options);
        }

        private TimeSpan ParseDuration(string input)
        {
            MatchCollection matches = Regex.Matches(input, @"(\d+)\s*days?|\b(\d+)\s*hours?");
            if (matches.Count == 0)
            {
                return TimeSpan.FromMinutes(0);
            }
            int days = 0;
            int hours = 0;
            foreach (Match match in matches)
            {
                if (match.Groups[1].Success)
                {
                    days += int.Parse(match.Groups[1].Value);
                }
                else if (match.Groups[2].Success)
                {
                    hours += int.Parse(match.Groups[2].Value);
                }
            }

            return TimeSpan.FromDays(days) + TimeSpan.FromHours(hours);
        }

        private void PlayNotificationSound()
        {
            try
            {
                string soundFilePath = "notification.wav";
                using (SoundPlayer player = new SoundPlayer(soundFilePath))
                {
                    player.Play();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error playing notification sound: {ex.Message}");
            }
        }

        public static string InsertBeforeWord(string originalString, string wordToInsertBefore, string insertion)
        {
            int index = originalString.IndexOf(wordToInsertBefore, StringComparison.OrdinalIgnoreCase);

            if (index != -1)
            {
                return originalString.Insert(index, insertion);
            }

            return originalString;
        }

        private async void startButton_Click(object sender, EventArgs e)
        {
            startButton.Enabled = false;
            button2.Enabled = true;

            await Task.Run(() =>
            {
                string email = "anneestherr45@gmail.com";
                string password = "Esther@2027";
                InitializeDriver();
                driver.Navigate().GoToUrl("https://app.atlanticwriters.com/login");
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                IWebElement emailInput = wait.Until(ExpectedConditions.ElementIsVisible(By.Name("email")));
                IWebElement passwordInput = driver.FindElement(By.Name("password"));
                emailInput.SendKeys(email);
                passwordInput.SendKeys(password);
                IWebElement button = driver.FindElement(By.XPath("//button[contains(@class, 'noselect')]"));
                button.Click();

                List<string> firstMessages = new List<string>
               {
                "Hello! I've thoroughly assessed your task and I'm confident I can work on it promptly and deliver way ahead before your deadline. Please note that I will ensure that the work is 100% original and free from plagiarism. Choose me for quality work!",
                "Hello! After reviewing your task, I'm certain I can handle it efficiently and deliver well ahead of schedule. Rest assured, the work will be entirely original and plagiarism-free. Opt for quality - choose me!",
                "Hi there! I've looked over your task and I'm ready to dive in, ensuring timely delivery well before your deadline. Originality and freedom from plagiarism are guaranteed. Trust me for top-notch work!",
                "Hey! Your task has been carefully assessed, and I'm ready to get started promptly to ensure early delivery. Expect 100% original, plagiarism-free content. Choose quality, choose me!",
                "Hello! Having reviewed your task, I'm confident in my ability to complete it swiftly and deliver well in advance. You can rely on me for original, plagiarism-free work. Choose excellence!",
                "Hello! Your task has my full attention, and I'm committed to delivering it well before your deadline. Rest assured, the work will be original and plagiarism-free. Choose excellence!",
                "Hello! I've gone through your task and I'm ready to work on it promptly, ensuring delivery ahead of schedule. Count on me for original, plagiarism-free work. Choose quality, choose me!",
                "Hi there! Having reviewed your task, I'm prepared to tackle it promptly and deliver well before your deadline. Expect nothing but original, plagiarism-free content. Choose excellence!",
                "Hey there! Your task is in good hands. I'm ready to work on it promptly and deliver ahead of schedule. Expect 100% original, plagiarism-free content. Choose me for quality assurance!",
                "Hello! Your task has been reviewed, and I'm ready to tackle it promptly to ensure timely delivery. Trust me for original, plagiarism-free work. Choose quality, choose me!",
                "Hello! After assessing your task, I'm confident in my ability to deliver well ahead of your deadline. Expect nothing but original, plagiarism-free content. Choose me for top-notch results!",
                "Hello! Your task has my full attention, and I'm committed to delivering it well before your deadline. Rest assured, the work will be original and plagiarism-free. Choose excellence!",
                "Hi! I've reviewed your task thoroughly and I'm ready to dive in, ensuring early delivery. Count on me for original, plagiarism-free work. Choose quality, choose me!",
                "Hey! Having reviewed your task, I'm prepared to work on it promptly and deliver well ahead of schedule. Expect 100% original, plagiarism-free content. Choose me for quality assurance!",
                "Hello! Your task is my priority, and I'm dedicated to completing it promptly, ensuring timely delivery. Trust me for original, plagiarism-free work. Choose excellence!",
                "Hello! I've carefully assessed your task and I'm prepared to work on it promptly, guaranteeing early delivery. Count on me for original, plagiarism-free content. Choose quality!",
                "Hello! After reviewing your task, I'm confident in my ability to deliver well before your deadline. Rest assured, the work will be entirely original and plagiarism-free. Choose me for superior quality!",
                "Hi there! Your task has my full attention, and I'm committed to delivering it well before your deadline. Expect nothing but original, plagiarism-free work. Choose excellence!",
                "Hey! I've reviewed your task thoroughly and I'm ready to dive in, ensuring prompt delivery. Trust me for original, plagiarism-free content. Choose quality, choose me!",
                "Hello! Your task has been reviewed, and I'm prepared to work on it promptly to ensure early delivery. Expect 100% original, plagiarism-free work. Choose me for top-notch results!"
            };

                List<string> secondMessages = new List<string>
            {
                "You can rely on me to deliver a paper that reflects professionalism and excellence.",
                "Expect nothing less than a thoroughly researched and expertly written paper from me.",
                "My commitment is to provide you with nothing short of excellence in your paper.",
                "Rest assured, I'm dedicated to delivering a top-notch paper for you.",
                "I'm fully devoted to crafting a paper of the highest quality for your satisfaction.",
                "You can count on me to deliver a paper that meets the highest standards of quality.",
                "I'm here to guarantee that your paper will reflect the utmost quality and expertise.",
                "Your paper will be thoroughly crafted to ensure it meets the standards of excellence.",
                "I'm determined to deliver a paper that exemplifies quality in every aspect.",
                "Quality is my priority, and I'm dedicated to delivering nothing but the best in your paper.",
                "I'm committed to providing you with a paper that exceeds your expectations in quality.",
                "I'm fully committed to ensuring that your paper meets the highest quality standards.",
                "Quality is non-negotiable - your paper will be nothing short of exceptional.",
                "Count on me to deliver a paper that is thorough, well-researched, and of top quality.",
                "I promise to deliver a paper that demonstrates the highest level of quality and expertise.",
                "Your paper will be crafted with precision and attention to detail to ensure its quality.",
                "I'm dedicated to providing you with a paper that reflects the pinnacle of quality.",
                "You can trust me to deliver a paper that is not just good, but outstanding in quality.",
                "Quality craftsmanship is at the heart of what I do, and your paper will reflect that."
            };

                int counter = 0;
                IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
                string lastElementOrderNumber = null;
                string currentOrderNumber = null;

                while (isRunning)
                {
                    if (!isRunning)
                    {
                        driver.Quit();
                        break;
                    }

                    try
                    {
                        if (counter == 20)
                        {
                            counter = 0;
                        }

                        IWebElement linkOrderElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//body/div/div[@data-name='Reamaze']/div[@data-name='Size']/div[@data-name='Size']/div[@class='content']/div[@data-name='Size']/div[@data-name='page: /order/available']/div[@data-name='AvailableOrders Table']/div[@data-name='Scrollable/Vertical']/div[@class='fraction svelte-u286v3']/div[@data-name='ExpertOrderRow'][1]/div[@class='fraction svelte-u286v3']/div[1]/div[@data-name='Block']/a")));

                        IWebElement titleOfOrderElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//body/div/div[@data-name='Reamaze']/div[@data-name='Size']/div[@data-name='Size']/div[@class='content']/div[@data-name='Size']/div[@data-name='page: /order/available']/div[@data-name='AvailableOrders Table']/div[@data-name='Scrollable/Vertical']/div[@class='fraction svelte-u286v3']/div[@data-name='ExpertOrderRow'][1]/div[@class='fraction svelte-u286v3']/div[1]")));

                        string orderElementTopic = titleOfOrderElement.Text;

                        string hrefValue = linkOrderElement.GetAttribute("href");
                        Console.WriteLine("HERE IS THE ELEMENT");
                        Console.WriteLine(hrefValue);
                        currentOrderNumber = linkOrderElement.Text;
                        
                        IWebElement orderStatus = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//body/div/div[@data-name='Reamaze']/div[@data-name='Size']/div[@data-name='Size']/div[@class='content']/div[@data-name='Size']/div[@data-name='page: /order/available']/div[@data-name='AvailableOrders Table']/div[@data-name='Scrollable/Vertical']/div[@class='fraction svelte-u286v3']/div[@data-name='ExpertOrderRow']/div[@class='fraction svelte-u286v3']/div[@data-name='Bids/Status']")));

                        IWebElement timerElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//body/div/div[@data-name='Reamaze']/div[@data-name='Size']/div[@data-name='Size']/div[@class='content']/div[@data-name='Size']/div[@data-name='page: /order/available']/div[@data-name='AvailableOrders Table']/div[@data-name='Scrollable/Vertical']/div[@class='fraction svelte-u286v3']/div[@data-name='ExpertOrderRow']/div[@class='fraction svelte-u286v3']/div[8]/div[@data-name='Size']/div[@class='tooltip-wrapper']//span")));

                        string orderTimer = timerElement.GetAttribute("innerHTML");
                        Console.WriteLine("HERE IS THE SECOND ELEMENT");
                        TimeSpan duration = ParseDuration(orderTimer);
                        TimeSpan minimumDuration = TimeSpan.FromHours(12);

                        try
                        {
                            IWebElement childElement = orderStatus.FindElement(By.XPath("./*"));
                            continue;
                        }
                        catch (NoSuchElementException)
                        {
                            Console.WriteLine(hrefValue);
                            if (currentOrderNumber != lastElementOrderNumber)
                            {
                                Console.WriteLine(hrefValue);
                                string message = $"Please check order {currentOrderNumber}\nTopic: {orderElementTopic}\nLink: {hrefValue}";
                                Console.WriteLine(message);
                                PlayNotificationSound();
                                SendTelegramMessage(message);
                            }
                            lastElementOrderNumber = currentOrderNumber;
                            Console.WriteLine("Current: " + currentOrderNumber);
                            Console.WriteLine("Last: " + lastElementOrderNumber);
                            if (duration < minimumDuration)
                            {
                                continue;
                            }
                            linkOrderElement.Click();
                            Console.WriteLine("orderStatus does not have any child elements.");
                            IWebElement titleElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//body/div/div[@data-name='Reamaze']/div[@data-name='Size']/div[@data-name='Size']/div[@class='content']/div[@data-name='ExpertOrderView']/div[1]/div[@data-name='OrderViewHeader']/div[@data-name='Header Main']//div[@data-name='Topic']")));

                            IWebElement orderStatusElement = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//body/div/div[@data-name='Reamaze']/div[@data-name='Size']/div[@data-name='Size']/div[@class='content']/div[@data-name='ExpertOrderView']/div[1]/div[@data-name='OrderViewHeader']/div[@data-name='Header Main']//div[@data-name='Order Status']//span")));
                            string statusText = orderStatusElement.GetAttribute("innerHTML");
                            IWebElement biddingBtn = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//body/div/div[@data-name='Reamaze']/div[@data-name='Size']/div[@data-name='Size']/div[@class='content']/div[@data-name='ExpertOrderView']/div[2]/div[@data-name='Main']/div[@data-name='Right']//div[@data-name='BidForm']//div[@data-name='Buttons']")));
                            biddingBtn.Click();
                    
                            if (statusText == "Abandoned")
                            {
                                js.ExecuteScript("window.history.back()");
                                continue;
                            }
                            try
                            {
                                System.Threading.Thread.Sleep(2000);
                                IWebElement chatBtn = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//body/div/div[@data-name='Reamaze']/div[@data-name='Size']/div[@data-name='Size']/div[@class='content']/div[@data-name='ExpertOrderView']/div[2]/div[@data-name='Main']/div[@data-name='Right']//button[@class='noselect svelte-m9fnhz']")));
                                chatBtn.Click();
                                IWebElement messageInput = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//body/div/div[@data-name='Reamaze']/div[@data-name='Size']/div[@data-name='Size']/div[@class='content']/div[@data-name='ExpertOrderView']/div[2]/div[@data-name='Main']/div[@data-name='Right']//div[@data-name='TextField']//textarea")));
                                string mesageInner = messageInput.GetAttribute("innerHTML");
                                string orderTopic = titleElement.GetAttribute("innerHTML") + " ";
                                string modifiedMessage = InsertBeforeWord(firstMessages[counter], "task", orderTopic);
                                messageInput.SendKeys(modifiedMessage);
                                IWebElement submitTextBtn = wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("//body/div/div[@data-name='Reamaze']/div[@data-name='Size']/div[@data-name='Size']/div[@class='content']/div[@data-name='ExpertOrderView']/div[2]/div[@data-name='Main']/div[@data-name='Right']//div[@data-name='Icons wrapper']")));
                                submitTextBtn.Click();
                                System.Threading.Thread.Sleep(10000);
                                submitTextBtn.Click();
                                //System.Threading.Thread.Sleep(5000);
                                //messageInput.SendKeys(secondMessages[counter]);
                                //Console.WriteLine(mesageInner);
                                //submitTextBtn.Click();
                                System.Threading.Thread.Sleep(2000);
                                js.ExecuteScript("window.history.back()");
                            }
                            catch (Exception ex) when (ex is NoSuchElementException ||
                                ex is StaleElementReferenceException ||
                                ex is ElementNotVisibleException || ex is TimeoutException)
                            {
                                js.ExecuteScript("window.history.back()");
                                continue;
                            }
                        }
                        counter++;
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }

                }
            });
            startButton.Enabled = true;
            button2.Enabled = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            isRunning = false;
            driver.Quit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            isRunning = false;
            driver.Quit();
            this.Close();
        }
    }
}
